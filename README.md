# BEM methodology

BEM  is smart way of naming your CSS classes to give them more transparency and meaning to other developers (and your future self). It is powerful and simple naming convention for front-end.

BEM is more strict (but flexible) and informative, which makes the BEM naming convention ideal for teams of developers on larger projects that might last a while.

Its primary purpose – providing meaningful, flexible, and reusable presentational/behavioural hooks for developers to use.

## Format

    Block__Element—modifier
    
    .block {}
    .block__element {}
    .block--modifier {}

Block - Standalone entity that is meaningful on its own. Example: container, menu and form etc.
Element - A part of a block that has no standalone meaning and is semantically tied to its block. Example: list item (li), caption and title.
Modifier - A flag on a block or element. Use them to change appearance or behaviour. Example: disabled (button), size and colour etc. 

# Why BEM?

### Modularity

No problem with cascading (specificity) // Transfer block between Project.

## Reuability

All Icon and have same width and height, but apply different colour with modifier.

## Structure

4 or 5 developer will have same understanding how the application has been styled. Avoid Specificity issue, spaghetti code, duplicity. 

# Alternatives

[https://en.bem.info/methodology/html/](https://en.bem.info/methodology/html/) - BEM -  Well documented, Used by CSS experts.

[http://oocss.org/](http://oocss.org/) - Object-Oriented CSS - object-based coding method - nesting

[https://smacss.com/](https://smacss.com/) - Scable and Modular Architecture for CSS - 2nd most used. Well documented.

AtomicCSS - Not well documented.

# Example One

    .person {}
    .hand {} // what of what? Person's hand, Clock hand, Hand in Card Game? // no context
    .male {} // what is female? Gender, Person, Animal? // no context
    .male-hand {}
    .left-hand {}
    
    // obscure - Not clear
    
    .person {}
    .person__hand {}
    .person--male {}
    .person--male__hand {}
    .person__hand--left {}
    
    // lot more explicit - some what clear
    

# Example Two

    <form class="site-search  full">
        <input type="text" class="field">
        <input type="Submit" value ="Search" class="button">
    </form>
    
    <form class="site-search  site-search--full">
        <input type="text" class="site-search__field">
        <input type="Submit" value ="Search" class="site-search__button">
    </form>

# Example Three

    <button class="button">
    	Normal button
    </button>
    <button class="button button--state-success">
    	Success button
    </button>
    <button class="button button--state-danger">
    	Danger button
    </button>
    
    Success button
    </button>
    <button class="button button--state-danger">
    	Danger button
    </button>
    
    -----
    
    CSS
    .button {
    	display: inline-block;
    	border-radius: 3px;
    	padding: 7px 12px;
    	border: 1px solid #D5D5D5;
    	background-image: linear-gradient(#EEE, #DDD);
    	font: 700 13px/18px Helvetica, arial;
    }
    .button--state-success {
    	color: #FFF;
    	background: #569E3D linear-gradient(#79D858, #569E3D) repeat-x;
    	border-color: #4A993E;
    }
    .button--state-danger {
    	color: #900;
    }
    
    -----
    
    .site-logo {}
    .site-logo--xmas {}

# Tips

Use scss (Sassy CSS) or sass (Syntactically awesome style sheets).

Use hyphenated casing.

Minify CSS and Gzip.

Write css properties and value Alphabetically. (Avoid duplicity)

# Arguments

- BEM is ugly!

Yes, power and readability it brings outweight the negatives.

- Too Much Typing!

Use autocomplete in editor and minify and gzip.

Overall: Power > Negatives

Please refer: [http://getbem.com/faq/](http://getbem.com/faq/) for Frequently Asked Questions

# Additional Points:

Nesting might lead to trouble (Depending on order, specificity and 

SASS, complied CSS look exactly as it should.

CSS does not gives error for multiple ids and classes (In case of big project)

There's no right way. Different people or company might approach this differently. Not a 100% solution, but at least provides the standard approach.

BEM or similar methodology is use by most of CSS experts Harry Roberts (CSSwizardary) and Nicolas Gallagher. Jen Simmons also recommends some front naming convention incase of big project.

Almost all CSS frameworks use some kind of naming convention (Check the elements in console) ( [http://bemskel.com/](http://bemskel.com/) )

# Companies using BEM:

Yandex (Search engine in Russia) - jetbrains (Dev tools for Devs)  - Alfabank

# Resources:

[https://css-tricks.com/specifics-on-css-specificity/](https://css-tricks.com/specifics-on-css-specificity/)

[https://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/](https://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/) - Harry Roberts

[https://twitter.com/intent/follow?original_referer=https%3A%2F%2Fcsswizardry.com%2Fcontact%2F&ref_src=twsrc^tfw&region=follow_link&screen_name=csswizardry&tw_p=followbutton](https://twitter.com/intent/follow?original_referer=https%3A%2F%2Fcsswizardry.com%2Fcontact%2F&ref_src=twsrc%5Etfw&region=follow_link&screen_name=csswizardry&tw_p=followbutton)

[http://nicolasgallagher.com/](http://nicolasgallagher.com/) - [https://twitter.com/necolas](https://twitter.com/necolas) -  [http://nicolasgallagher.com/about-html-semantics-front-end-architecture/](http://nicolasgallagher.com/about-html-semantics-front-end-architecture/)

[http://getbem.com/](http://getbem.com/)

# Related Tweets:

[https://twitter.com/jensimmons/status/978352380587708416](https://twitter.com/jensimmons/status/978352380587708416)

[https://twitter.com/csswizardry/status/732515193130000384](https://twitter.com/csswizardry/status/732515193130000384)

[https://twitter.com/MaOberlehner/status/1046348040729219072](https://twitter.com/MaOberlehner/status/1046348040729219072)

[https://twitter.com/ewald__b/status/937965714341842947](https://twitter.com/ewald__b/status/937965714341842947)

[https://twitter.com/iamdevloper/status/927913260451368960?lang=en](https://twitter.com/iamdevloper/status/927913260451368960?lang=en)